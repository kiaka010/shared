<?php

class CharUtils {

    /**
     * folderize
     * formats the string into our folder friendly format
     * (lowercase, no hyphens)
     * @param string $str
     * @return string
     */
    public static function folderize($str) {
        return preg_replace('/-/', '', strtolower($str));
    }

    /**
     * Function to convert hyphenated url string to CamelCase
     *
     * @param string $string
     * @return string
     * @author Adam Jessop
     */
    public static function convertHyphenatedToCamelCase($string) {

        //convert hyphenated to camel case
        if (strpos($string, '-') !== false) {
            $parts = explode('-', strtolower($string));
            $parts = array_map('ucfirst', $parts);
            $string = implode('', $parts);
        }

        return $string;
    }

    /**
     * camelize
     * @param string $string
     * @param boolean $pascalCase if true will lowercase first letter of string
     * @return string
     */
    public static function camelize($string, $pascalCase = false) {
        $string = str_replace(array('-', '_'), ' ', $string);
        $string = ucwords($string);
        $string = str_replace(' ', '', $string);

        if (!$pascalCase) {
            $string = strtolower($string[0]) . substr($string, 1);
            return $string;
        }
        return $string;
    }	
    
	public static function cleanTitle($title) { 
        $cleanTitle = str_replace("&#39;", "", $title);
        $cleanTitle = preg_replace("#([^a-z0-9 \-\_])#i", '', strtolower(trim($cleanTitle))); //get rid of anything that isn't alphanumeric or a space/dash/underscore.
        $cleanTitle = str_replace(array(' ','_'), '-', $cleanTitle); //spaces and underscores become dashes
        $cleanTitle = preg_replace("#([\-])+#", '-', $cleanTitle); //multiple hyphens become one.
        $cleanTitle = trim($cleanTitle, '-'); //trim leading/trailing hyphens
		return $cleanTitle;
	}

}

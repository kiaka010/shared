<?php

class Upload {

    protected $uploadOk;
    protected $uploadDir;
    protected $fileName;
    protected $errorMessage = null;

    protected function generateError() {
        return ['error' => $this->errorMessage];
    }

    protected function generateSuccess() {
        return ['success' => true];
    }

    public function setUploadDir($uploadDir) {
        $this->uploadDir = $uploadDir;
    }

    public function setFileName($fileName) {
        $this->fileName = $fileName;
    }

    protected function getTargetFile() {
        return $this->uploadDir . $this->fileName;
    }

    public function run() {

        $this->checkIsImage();
        $this->checkFileExists();
        return $this->move();
    }

    protected function move() {
        // Check if $uploadOk is set to 0 by an error
        if ($this->uploadOk == 0) {
            !isset($this->errorMessage) ? $this->errorMessage = "Sorry, your file was not uploaded." : null;
            return $this->generateError();
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $this->getTargetFile())) {
                return $this->generateSuccess();
            } else {
                !isset($this->errorMessage) ? $this->errorMessage = "Sorry, there was an error uploading your file." : null;
                return $this->generateError();
            }
        }
    }

    protected function checkIsImage() {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
//            $this->errorMessage = "File is an image - " . $check["mime"] . ".";
            $this->uploadOk = 1;
        } else {
            $this->errorMessage = "File is not an image.";
            $this->uploadOk = 0;
        }
    }

    protected function checkFileExists() {
        // Check if file already exists
        if (file_exists($this->getTargetFile())) {
            $this->errorMessage = "File already exists.";
            $this->uploadOk = 0;
        }
    }

    /**
     * not used at the moment
     * @param type $size
     */
    protected function checkiSFize($size = 500000) {
        if ($_FILES["fileToUpload"]["size"] > $size) {
            echo "Sorry, your file is too large.";
            $this->uploadOk = 0;
        }
    }

    /**
     * Not used at the moment
     */
    protected function checkFileTypes() {
        $imageFileType = pathinfo($this->getTargetFile(), PATHINFO_EXTENSION);

        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
    }

}

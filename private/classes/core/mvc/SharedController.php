<?php

class SharedController {
    public $isLogedIn = false;

    public $defaultPromos = [];

    public $navToken = null;
    public $moduleClass = null;
    public function _preDispatch() {
        $this->resolveModuleClass();
        if(isset($this->moduleClass)){
            if(method_exists($this->moduleClass, '_preDispatch'))
               $this->moduleClass->_preDispatch();
        }
    }

    private function resolveModuleClass() {
        $subModuleFileName = sprintf('%s' . DS . 'Site%sController.php',
            CharUtils::folderize($this->request->getModule()),
            CharUtils::camelize($this->request->getModule(), true)
                . ucwords($this->request->getSubDir())
        );

        $fileName = sprintf('%s' . DS . 'Site%sController.php',
            CharUtils::folderize($this->request->getModule()),
            CharUtils::camelize($this->request->getModule(), true)
        );

        $className = 'Site' . CharUtils::camelize($this->request->getModule(), true) . 'Controller';

        $class = null;
        if (file_exists(DOC_ROOT . '/site/' . $subModuleFileName)) {
            require_once DOC_ROOT . '/site/' . $subModuleFileName;
            $className .= ucwords($this->request->getSubDir());
            $class = new $className($this);
        } else if (file_exists(DOC_ROOT . '/site/' . $fileName)) {

            require_once DOC_ROOT . '/site/' . $fileName;
            $class = new $className($this);
        }

        if ($class) {
            $this->moduleClass = $class;
        }

    }
    public function _postDispatch() {
        if(isset($this->moduleClass)){
            if(method_exists($this->moduleClass, '_postDispatch'))
               $this->moduleClass->_postDispatch();
        }
        $promos = $this->getPromoClass();
    }

    protected function getPromoClass() {
        return new SharedPromos($this->defaultPromos);
    }

    /**
     * setConfig
     *
     * @param $config the config to attach
     */
    public function setConfig($config) {
        $this->config = $config;
    }

    /**
     * setResponse
     *
     * Setter for SkySportsResponse object
     * @param object $response
     */
    public function setResponse($response) {
        if (is_object($response))
            $this->response = $response;
    }

    /**
     * setRequest
     *
     * Setter for SiteRequest object
     * @param object $request
     */
    public function setRequest($request) {
        if (is_object($request))
            $this->request = $request;
    }

    public function dispatch($methodName, $params = array(), $isAjax = false) {

        # try and load the correct consts file
        $filename = 'private/classes/' . CharUtils::folderize($this->request->getModule()) . '/' . CharUtils::camelize($this->request->getModule(), true) . 'Consts.php';

        if (file_exists($filename)) {
            require_once($filename);
            $className = CharUtils::camelize($this->request->getModule(), true) . 'Consts';
            if (class_exists($className))
                $this->consts = new $className;
        }

        // set global view var of request
        $this->response->assignVar('request', $this->request);

        call_user_func_array(array($this, '_preDispatch'), $params);

        $response = call_user_func_array(array($this, $methodName), $params);

         call_user_func_array(array($this, '_postDispatch'), $params);

        return $response;
    }

    /**
     * addView
     *
     * Add a view to the SkySportsResponse view stack
     * @param string $name name of the view
     * @param string $template path to the template
     * @param mixed $data data to be passed to the view
     * @param array $params parameters associated with the view
     */
    public function addView($name, $template, $data = null, $params = array()){
        $this->response->addView($name, $template, $data, $params);
    }

    /**
     * addHtmlView
     *
     * Add html as a view to the SkySportsResponse view stack
     * @param string $name name of the view
     * @param string $html rendered HTML
     * @param array $params parameters associated with the view
     */
    public function addHtmlView($name = null, $html,  $params = array()){
        $this->response->addHtmlView($name, $html, $params);
    }

    /**
     * fetchViewHtml
     *
     * Render a view object to get the HTML
     * @param string $template path the the template
     * @param mixed $data data to pass to the view
     * @return string rendered HTML
     */
    public function fetchViewHtml($template, $data = array()){
        if(is_string($template))
            return $this->response->fetchViewHtml($template, $data);
    }

}


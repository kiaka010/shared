<?php

class SharedNavElement {

    protected $url;
    protected $name;
    protected $isActive = false;
    protected $subMenu;
    protected $isLast = false;
    protected $isRight = false;

    public function setUrl($url) {
        $this->url = $url;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function validateActivePage($activePageName) {
        $this->isActive = $activePageName == $this->name;
    }

    public function isActivePage() {
        return $this->isActive;
    }

    public function addSubMenuElement(NavElement $subMenu) {
        $this->subMenu[] = $subMenu;
    }

    public function getLastSubMenuElement() {
        return $this->subMenu[count($this->subMenu) - 1];
    }

    public function hasSubMenu() {
        return !empty($this->subMenu);
    }

    public function getSubMenu() {
        return $this->subMenu;
    }

    public function setLast($switch) {
        $this->isLast = $switch;
    }

    public function isLast() {
        return $this->isLast;
    }
    
    public function setIsRight($isRight) {
        if(is_bool($isRight)) {
            $this->isRight = $isRight;
        }
    }
    
    public function isRight() {
        return $this->isRight;
    }
    
    
}

<?php

class SharedNav {

    protected $activePage;
    protected $reverseOrder = false;

    public function setActivePage($activePage) {
        $this->activePage = $activePage;
    }

    protected function buildNavElement($name, $href, $subMenu = [], $isRight = false) {
        $navElement = new NavElement();
        $navElement->setUrl($href);
        $navElement->setName($name);
        $navElement->validateActivePage($this->activePage);
        $navElement->setIsRight($isRight);

        if (!empty($subMenu) && is_array($subMenu)) {
            foreach ($subMenu as $subMenuElement) {

                $navElement->addSubMenuElement(
                        $this->buildNavElement(
                                $subMenuElement[0], $subMenuElement[1]
                        )
                );
            }
            $navElement->getLastSubMenuElement()->setLast(true);
        }
        return $navElement;
    }

    public function setReverseOrder($reverse) {
        $this->reverseOrder = $reverse;
    }

}

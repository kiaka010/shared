<?php
return array(
    
    array('/json-test/?', array('module' => 'homepage', 'controller' => 'index' , 'action' => 'json_test')),
    array('/links/?', array('module' => 'homepage', 'controller' => 'index' , 'action' => 'chapterLinks')),
//    array('/members/?', array('module' => 'character', 'controller' => 'profile' , 'action' => 'index')),
    //DEFAULTS
    array('/[s:module]?/?[s:controller]?/?[s:action]?/?[*:params]?'),
    array('/[s:module]?/?[s:controller]?/?[s:action]?/?[*:params]?',null, 'POST')

);
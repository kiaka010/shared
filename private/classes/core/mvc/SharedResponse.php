<?php

class SharedResponse {

    protected static $globalData;
    public static $instance;
    protected $views = array();
    protected $currentRootView = 'root';
    
    /**
     * Singleton method to ensure that there is only ever one instance of this class
     * @return object this class
     */
    public static function getInstance(){
        if(!self::$instance instanceof SharedResponse) {
            //create an instance of this class
            self::$instance = new SharedResponse();
        }
       //return single instance
        return self::$instance;
    }
    /**
     * Function to add a view to the frontend stack
     * @param string $name name of the view
     * @param string $template location of template file
     * @param mixed $data data to be passed to the view when rendering
     * @param array $params array of params. Current options: insertBefore, insertAfter
     * @throws SkySportsError
     */
    public function addView($name, $template, &$data, $params = array()){
        //if we don't have a name, lets generate one
        if(empty($name))
            $name = get_class($this).'-'.$this->viewCount++;

        //if we don't have a template, throw an error, as we kind of need one for this function
        if(empty($template) || !is_string($template))
            throw new Exception('Invalid template defined : "'.$template.'"', E_USER_NOTICE);

        $view = $this->initialiseView($name, $params);
        //assign view data if we have any
        if(!empty($data))
            $view->setData($data);

        //set the view template
        $view->setTemplate($template);

        //add the view on the end of the stack for now.
        $this->addViewToStack($name, $view);



    }
    /**
     * Function to add already rendered html to the view stack
     * @param string $name name of the view
     * @param string $html the pre-rendered HTML
     * @param array $params parameters to be passed to the view
     */
    public function addHtmlView($name, $html, $params = array()){
        // if we don't have a name, generate one
         if(empty($name))
            $name = get_class($this).'-'.$this->viewCount++;

         //if there is no HTML then we don't need to do anything
         if(empty($html))
             return;

         //create new SkySportsViewObject
         $view = $this->initialiseView($name, $params);
         //set the output to the rendered HTML
         $view->setOutput($html);
         //Set the view as rendered
         $view->setRendered(true);
         //Add the view to the end of the stack for now
         $this->addViewToStack($name, $view);

    }

    private function initialiseView($name, $params){

         //create new SkySportsView object
        $view = new SiteView();

        //set the view params
        $view->setParams($params);

        //set the view name
        $view->setViewName($name);
        
        //set the parent
        if(isset($params['parent'])){
            $view->setParentView($params['parent']);
        }else{
            $view->setParentView($this->currentRootView);
        }

        //set current root if told to
        if(isset($params['setAsRoot']) && $params['setAsRoot'] == true)
            $this->currentRootView = $name;

        return $view;
    }

    /**
     * Render a view and return the HTML
     * @param string $template the path to the template
     * @param mixed $data used within the template
     * @return string HTML of rendered view
     */
    public static function fetchViewHtml($template, $data){
        //Create new SkySportsView object
        $view = new SiteView();
        //set view template
        $view->setTemplate($template);
        //set view data
        $view->setData($data);
        //render the template with the current global data
        return $view->render(self::$globalData);
    }

    /**
     * Render the frontend response.
     * Outputs the complete HTML
     */
    public function renderResponse(){
        //need to make sure our views are in order
        $this->orderViews();
        //initialise HTML string
        $html = '';
        if(!empty($this->views['root'])) {
            //loop through the view stack
            foreach($this->views['root'] as $view){
                //render each view and add the HTML string
                $html.= $view->render(self::$globalData);
            }
        }
        
        //output the HTML to the browser
        echo $html;
    }

    /**
     * Helper function to order the view stack based on
     * insertBefore or insertAfter parameters
     */
    private function orderViews(){
        //first we need to order in terms of insertBefore and After
        foreach($this->views as $name => $view){
            //do we need to insert this view before another?
            if($view->getInsertBefore() != ""){
                //insert the current view before the named view
                $this->insertViewBefore($name, $view, $view->getInsertBefore());
            //do we need to insert this view after another?
            }elseif($view->getInsertAfter() != ""){
                //insert the current view after the named view
                $this->insertViewAfter($name, $view, $view->getInsertAfter());
            }
        }
        //next we need to stack into associative parents
        //initialise vars
        $newViewsStack = array();
        $parent = 'root';
        foreach($this->views as $name => $view){
            if($view->getInsertBefore() != "" && isset($this->views[$view->getInsertBefore()])){
                $parent = $this->views[$view->getInsertBefore()]->getParentView();
            }
            //I only want to stack if they haven't been ordered, as that takes priority
            if($view->getInsertBefore() == "" && $view->getInsertAfter() == ""){
                $parent = $view->getParentView();
            }
            $newViewsStack[$parent][] = $view;
        }
        $this->views = $newViewsStack;
    }

    /**
     * Helper function to insert a view before another
     * @param string $name the view name to be positioned
     * @param object $view the view to be moved
     * @param string $positionedViewName the named view that I am stacking on top of
     */
    private function insertViewBefore($name, $view, $positionedViewName){
       //unset as it will get re-inserted
       unset($this->views[$name]);
       //lets try and find the named view position in the stack
       $pos = $this->getViewArrayPosition($positionedViewName);

       if($pos === false)
           return $this->addViewToStack($name, $view);

       //have to use array slice method so to preserve keys
       $this->views = array_slice($this->views, 0, $pos, true) +
                      array($name => $view) +
                      array_slice($this->views, $pos, null, true);
    }

    private function insertViewAfter($name, $view, $positionedViewName){
       //unset as it will get re-inserted
       unset($this->views[$name]);
       //lets try and find the named view position in the stack
       $pos = $this->getViewArrayPosition($positionedViewName);

       if($pos === false)
           return $this->addViewToStack($name, $view);

       //increment by one so it is stacked after.
       $pos++;

       //have to use array slice method so to preserve keys
       $this->views = array_slice($this->views, 0, $pos, true) +
                      array($name => $view) +
                      array_slice($this->views, $pos, null, true);
    }

    private function insertViewAsChild($name, $view, $parent){

    }

    /**
     * Add a view on to the end of the view stack
     * @param string $name The name of the view
     * @param object $view The view object
     */
    private function addViewToStack($name, $view, $num = 2) {

        if (isset($this->views[$name])) {
            if (isset($this->views[$name . '-' . $num])) {
                $this->addViewToStack($name, $view, ++$num);
                return;
            } else {
                $name = $name . '-' . $num;
                $view->setViewName($name);
            }
        }

        $this->views[$name] = $view;
    }

    /**
     * Try and find a view position by its name
     * @param type $viewName The name of the view
     * @return int the position of the view
     */
    public function getViewArrayPosition($viewName){
        $i = array_search($viewName, array_keys($this->views));
        return $i;
    }

    /**
     * helper function to override the data already passed to a view
     * @param string $viewName The name of the view
     * @param mixed $data the new data
     * @return boolean false if unable to find view
     */
    public function setViewData($viewName, $data){
        //find view
        if(!is_string($viewName) || !(is_array($data) || is_object($data)))
                return false;
        if(!array_key_exists($viewName, $this->views))
                return false;

        $this->views[$viewName]->setData($data);
    }

    /**
     * function to add data to an existing view (will overwrite existing duplicate keys)
     * @param string $viewName
     * @param mixed $data
     * @return boolean success
     */
    public function addViewData($viewName, $data){
        if(!(is_array($data) || is_object($data)))
                return false;
        if(!array_key_exists($viewName, $this->views))
                return false;


        return $this->views[$viewName]->addData($data);
    }

    /**
     * Set global data to be used across all views when rendered
     * @param string $varName name of access variable
     * @param mixed $data
     */
    public function assignVar($varName, &$data){
        self::$globalData[$varName] = $data;
    }

    /**
     * get global data var
     * @param string $varName name of access variable
     * @param mixed $data
     */
    public static function getVar($varName){
        if (!empty(self::$globalData[$varName]))
            return self::$globalData[$varName];
    }

    /**
     * Sets response content type header
     * @param string $header
     */
    public function setHeaderContentType($header) {
        if(is_string($header))
            $this->headerContentType = 'Content-type: '.$header;
    }
    /**
     * Gets the response content type header
     * @return string
     */
    public function getHeaderContentType() {
        return $this->headerContentType;
    }
    /**
     * Sets the current root view var
     * @param string $view
     */
    public function setCurrentRootView($view){
        if(is_string($view))
            $this->currentRootView = $view;
    }
    /**
     * Gets the current root view var
     * @return string
     */
    public function getCurrentRootView(){
        return $this->currentRootView;
    }

    /**
     * Renders the child views within a wrapping template
     * @param string $viewName
     * @return string html
     */
    public function renderChildViews($viewName){
        $html = '';
        if(isset($this->views[$viewName])){
            foreach($this->views[$viewName] as $view){
                $html.= $view->render(self::$globalData);
            }
        }
        return $html;
    }

    /**
     * Renders the given child view
     * @param string $parentViewName Where to look for the child
     * @param string $childViewName View to render
     * @param bool $remove Whether to remove the view after rendering.  If not
     *      it will be rendered in a call to the renderChildViews method.
     *      Defaults to true
     * @return string html
     */
    public function renderChildView($parentViewName, $childViewName, $remove = true) {
        $html = '';
        if (isset($this->views[$parentViewName])) {
            foreach($this->views[$parentViewName] as $idx => $view){
                if ($view->getViewName() == $childViewName) {
                    $html .= $view->render(self::$globalData);
                    if ($remove) {
                        unset($this->views[$parentViewName][$idx]);
                    }
                    break;
                }
            }
        }
        return $html;
    }

    /**
     * Sets the parent of a view
     * @param string $targetView
     * @param string $parentView
     */
    public function setViewParent($targetView, $parentView){
        if(is_string($targetView) && is_string($parentView) && isset($this->views[$targetView])){
            $this->views[$targetView]->setParentView($parentView);
        }
    }
    
    public function removeViewParent($targetView){
        if(is_string($targetView) && isset($this->views[$targetView])){
            $this->views[$targetView]->removeParentView();
        }
    }
    protected $cacheDuration = 300;
    public function fetchCache() {
        return $this->cacheDuration;
    }
    
    public function setCache($time){
        $this->cacheDuration = $time;
    }

    /**
     * Sets the response cache duration
     * @param integer $duration
     */
//    public function setCacheDuration($duration) {
//        if (isset($duration) && is_numeric($duration))
//            $this->cacheDuration = $duration;
//    }

    /**
     * Builds the page cache header
     */
//    public function buildCache() {
//        $this->cacheHelper->setPageCacheDurationHeader($this->cacheDuration);
//    }

    /**
     * Removes a view from the view stack
     * @todo need to check if it has children, elevate them to the next parent
     * @param string $name
     */
    public function removeView($name){
        if(is_string($name) && isset($this->views[$name]))
           unset($this->views[$name]);
    }

    /**
     * Clear the views array
     */
    public function clearViews(){
        $this->views = array();
    }
}


//
//require_once CLASS_PATH.'/core/SkySportsView.php';
//class SkySportsResponse{
//    /**
//     * @var object instance of $this - singleton
//     */
//    public static $instance;
//
//    /**
//     * Associative array of views stacked by parent
//     * and order
//     * @var array
//     */
//    protected $views = array();
//
//    /**
//     * @var int helper var for generating names of views
//     */
//    protected $viewCount = 0;
//
//    /**
//     * @var array global data shared across all views
//     */
//    protected static $globalData;
//
//    /**
//     * @var string header information
//     */
//    protected $headerContentType = null;
//
//    /**
//     * @var string view that is acting as the current parent
//     */
//    protected $currentRootView = 'root';
//
//    /**
//     * @var object for cacheHelper
//     */
//    protected $cacheHelper;
//
//    /**
//     * @var int for default cache duration
//     */
//    private $cacheDuration = null;
//    /**
//     * Singleton method to ensure that there is only ever one instance of this class
//     * @return object this class
//     */
//    public static function getInstance(){
//        if(!self::$instance instanceof SkySportsResponse) {
//            //create an instance of this class
//            self::$instance = new SkySportsResponse();
//        }
//       //return single instance
//        return self::$instance;
//    }
//
//    /**
//     * construct magic method declared as private for singleton implementation
//     */
//    private function __construct() {
//        //set the cacheHelper
//        $this->cacheHelper = new CacheHelper(new SiteCachePolicy());
//    }
//
//    /**
//     * Function to add a view to the frontend stack
//     * @param string $name name of the view
//     * @param string $template location of template file
//     * @param mixed $data data to be passed to the view when rendering
//     * @param array $params array of params. Current options: insertBefore, insertAfter
//     * @throws SkySportsError
//     */
//    public function addView($name, $template, &$data, $params = array()){
//        //if we don't have a name, lets generate one
//        if(empty($name))
//            $name = get_class($this).'-'.$this->viewCount++;
//
//        //if we don't have a template, throw an error, as we kind of need one for this function
//        if(empty($template) || !is_string($template))
//            throw new Exception('Invalid template defined : "'.$template.'"', E_USER_NOTICE);
//
//        $view = $this->initialiseView($name, $params);
//
//        //assign view data if we have any
//        if(!empty($data))
//            $view->setData($data);
//
//        //set the view template
//        $view->setTemplate($template);
//
//        //add the view on the end of the stack for now.
//        $this->addViewToStack($name, $view);
//
//
//
//    }
//    /**
//     * Function to add already rendered html to the view stack
//     * @param string $name name of the view
//     * @param string $html the pre-rendered HTML
//     * @param array $params parameters to be passed to the view
//     */
//    public function addHtmlView($name, $html, $params = array()){
//        // if we don't have a name, generate one
//         if(empty($name))
//            $name = get_class($this).'-'.$this->viewCount++;
//
//         //if there is no HTML then we don't need to do anything
//         if(empty($html))
//             return;
//
//         //create new SkySportsViewObject
//         $view = $this->initialiseView($name, $params);
//         //set the output to the rendered HTML
//         $view->setOutput($html);
//         //Set the view as rendered
//         $view->setRendered(true);
//         //Add the view to the end of the stack for now
//         $this->addViewToStack($name, $view);
//
//    }
//
//    private function initialiseView($name, $params){
//
//         //create new SkySportsView object
//        $view = new SkySportsView();
//
//        //set the view params
//        $view->setParams($params);
//
//        //set the view name
//        $view->setViewName($name);
//
//        //set the parent
//        if(isset($params['parent'])){
//            $view->setParentView($params['parent']);
//        }else{
//            $view->setParentView($this->currentRootView);
//        }
//
//        //set current root if told to
//        if(isset($params['setAsRoot']) && $params['setAsRoot'] == true)
//            $this->currentRootView = $name;
//
//        return $view;
//    }
//
//    /**
//     * Render a view and return the HTML
//     * @param string $template the path to the template
//     * @param mixed $data used within the template
//     * @return string HTML of rendered view
//     */
//    public static function fetchViewHtml($template, $data){
//        //Create new SkySportsView object
//        $view = new SkySportsView();
//        //set view template
//        $view->setTemplate($template);
//        //set view data
//        $view->setData($data);
//        //render the template with the current global data
//        return $view->render(self::$globalData);
//    }
//
//    /**
//     * Render the frontend response.
//     * Outputs the complete HTML
//     */
//    public function renderResponse(){
//        //need to make sure our views are in order
//        $this->orderViews();
//
//        //initialise HTML string
//        $html = '';
//        if(!empty($this->views['root'])) {
//            //loop through the view stack
//            foreach($this->views['root'] as $view){
//                //render each view and add the HTML string
//                $html.= $view->render(self::$globalData);
//            }
//        }
//
//        //output the HTML to the browser
//        echo $html;
//    }
//
//    /**
//     * Helper function to order the view stack based on
//     * insertBefore or insertAfter parameters
//     */
//    private function orderViews(){
//        //first we need to order in terms of insertBefore and After
//        foreach($this->views as $name => $view){
//            //do we need to insert this view before another?
//            if($view->getInsertBefore() != ""){
//                //insert the current view before the named view
//                $this->insertViewBefore($name, $view, $view->getInsertBefore());
//            //do we need to insert this view after another?
//            }elseif($view->getInsertAfter() != ""){
//                //insert the current view after the named view
//                $this->insertViewAfter($name, $view, $view->getInsertAfter());
//            }
//        }
//        //next we need to stack into associative parents
//        //initialise vars
//        $newViewsStack = array();
//        $parent = 'root';
//        foreach($this->views as $name => $view){
//            if($view->getInsertBefore() != "" && isset($this->views[$view->getInsertBefore()])){
//                $parent = $this->views[$view->getInsertBefore()]->getParentView();
//            }
//            //I only want to stack if they haven't been ordered, as that takes priority
//            if($view->getInsertBefore() == "" && $view->getInsertAfter() == ""){
//                $parent = $view->getParentView();
//            }
//
//            $newViewsStack[$parent][] = $view;
//        }
//        $this->views = $newViewsStack;
//    }
//
//    /**
//     * Helper function to insert a view before another
//     * @param string $name the view name to be positioned
//     * @param object $view the view to be moved
//     * @param string $positionedViewName the named view that I am stacking on top of
//     */
//    private function insertViewBefore($name, $view, $positionedViewName){
//       //unset as it will get re-inserted
//       unset($this->views[$name]);
//       //lets try and find the named view position in the stack
//       $pos = $this->getViewArrayPosition($positionedViewName);
//
//       if($pos === false)
//           return $this->addViewToStack($name, $view);
//
//       //have to use array slice method so to preserve keys
//       $this->views = array_slice($this->views, 0, $pos, true) +
//                      array($name => $view) +
//                      array_slice($this->views, $pos, null, true);
//    }
//
//    private function insertViewAfter($name, $view, $positionedViewName){
//       //unset as it will get re-inserted
//       unset($this->views[$name]);
//       //lets try and find the named view position in the stack
//       $pos = $this->getViewArrayPosition($positionedViewName);
//
//       if($pos === false)
//           return $this->addViewToStack($name, $view);
//
//       //increment by one so it is stacked after.
//       $pos++;
//
//       //have to use array slice method so to preserve keys
//       $this->views = array_slice($this->views, 0, $pos, true) +
//                      array($name => $view) +
//                      array_slice($this->views, $pos, null, true);
//    }
//
//    private function insertViewAsChild($name, $view, $parent){
//
//    }
//
//    /**
//     * Add a view on to the end of the view stack
//     * @param string $name The name of the view
//     * @param object $view The view object
//     */
//    private function addViewToStack($name, $view, $num = 2) {
//
//        if (isset($this->views[$name])) {
//            if (isset($this->views[$name . '-' . $num])) {
//                $this->addViewToStack($name, $view, ++$num);
//                return;
//            } else {
//                $name = $name . '-' . $num;
//                $view->setViewName($name);
//            }
//        }
//        $log = 'Added view to response stack: ' . $view->getViewName();
//
//        if ($view->getInsertBefore() != null) {
//            $log .= " insert before ". $view->getInsertBefore();
//        } elseif ($view->getInsertAfter() != null) {
//            $log .= " insert after ". $view->getInsertAfter();
//        }
//        SkySportsLog::log($log);
//        $this->views[$name] = $view;
//    }
//
//    /**
//     * Try and find a view position by its name
//     * @param type $viewName The name of the view
//     * @return int the position of the view
//     */
//    public function getViewArrayPosition($viewName){
//        $i = array_search($viewName, array_keys($this->views));
//        return $i;
//    }
//
//    /**
//     * helper function to override the data already passed to a view
//     * @param string $viewName The name of the view
//     * @param mixed $data the new data
//     * @return boolean false if unable to find view
//     */
//    public function setViewData($viewName, $data){
//        //find view
//        if(!is_string($viewName) || !(is_array($data) || is_object($data)))
//                return false;
//        if(!array_key_exists($viewName, $this->views))
//                return false;
//
//        $this->views[$viewName]->setData($data);
//    }
//
//    /**
//     * function to add data to an existing view (will overwrite existing duplicate keys)
//     * @param string $viewName
//     * @param mixed $data
//     * @return boolean success
//     */
//    public function addViewData($viewName, $data){
//        if(!(is_array($data) || is_object($data)))
//                return false;
//        if(!array_key_exists($viewName, $this->views))
//                return false;
//
//
//        return $this->views[$viewName]->addData($data);
//    }
//
//    /**
//     * Set global data to be used across all views when rendered
//     * @param string $varName name of access variable
//     * @param mixed $data
//     */
//    public function assignVar($varName, &$data){
//        self::$globalData[$varName] = $data;
//    }
//
//    /**
//     * get global data var
//     * @param string $varName name of access variable
//     * @param mixed $data
//     */
//    public static function getVar($varName){
//        if (!empty(self::$globalData[$varName]))
//            return self::$globalData[$varName];
//    }
//
//    /**
//     * Sets response content type header
//     * @param string $header
//     */
//    public function setHeaderContentType($header) {
//        if(is_string($header))
//            $this->headerContentType = 'Content-type: '.$header;
//    }
//    /**
//     * Gets the response content type header
//     * @return string
//     */
//    public function getHeaderContentType() {
//        return $this->headerContentType;
//    }
//    /**
//     * Sets the current root view var
//     * @param string $view
//     */
//    public function setCurrentRootView($view){
//        if(is_string($view))
//            $this->currentRootView = $view;
//    }
//    /**
//     * Gets the current root view var
//     * @return string
//     */
//    public function getCurrentRootView(){
//        return $this->currentRootView;
//    }
//
//    /**
//     * Renders the child views within a wrapping template
//     * @param string $viewName
//     * @return string html
//     */
//    public function renderChildViews($viewName){
//        $html = '';
//        if(isset($this->views[$viewName])){
//            foreach($this->views[$viewName] as $view){
//                $html.= $view->render(self::$globalData);
//            }
//        }
//        return $html;
//    }
//
//    /**
//     * Renders the given child view
//     * @param string $parentViewName Where to look for the child
//     * @param string $childViewName View to render
//     * @param bool $remove Whether to remove the view after rendering.  If not
//     *      it will be rendered in a call to the renderChildViews method.
//     *      Defaults to true
//     * @return string html
//     */
//    public function renderChildView($parentViewName, $childViewName, $remove = true) {
//        $html = '';
//        if (isset($this->views[$parentViewName])) {
//            foreach($this->views[$parentViewName] as $idx => $view){
//                if ($view->getViewName() == $childViewName) {
//                    $html .= $view->render(self::$globalData);
//                    if ($remove) {
//                        unset($this->views[$parentViewName][$idx]);
//                    }
//                    break;
//                }
//            }
//        }
//        return $html;
//    }
//
//    /**
//     * Sets the parent of a view
//     * @param string $targetView
//     * @param string $parentView
//     */
//    public function setViewParent($targetView, $parentView){
//        if(is_string($targetView) && is_string($parentView) && isset($this->views[$targetView])){
//            $this->views[$targetView]->setParentView($parentView);
//        }
//    }
//
//    /**
//     * Sets the response cache duration
//     * @param integer $duration
//     */
//    public function setCacheDuration($duration) {
//        if (isset($duration) && is_numeric($duration))
//            $this->cacheDuration = $duration;
//    }
//
//    /**
//     * Builds the page cache header
//     */
//    public function buildCache() {
//        $this->cacheHelper->setPageCacheDurationHeader($this->cacheDuration);
//    }
//
//    /**
//     * Removes a view from the view stack
//     * @todo need to check if it has children, elevate them to the next parent
//     * @param string $name
//     */
//    public function removeView($name){
//        if(is_string($name) && isset($this->views[$name]))
//           unset($this->views[$name]);
//    }
//
//    /**
//     * Clear the views array
//     */
//    public function clearViews(){
//        $this->views = array();
//    }
//}
?>

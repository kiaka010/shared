<?php 

class SharedView {
    /**
     * Data - currently public due to extending SSObject
     * @var mixed the data associated with this view
     */
    public $data;
    /**
     * @var boolean if this view has been rendered of not
     */
    protected $rendered = false;
    /**
     * @var string this views html ouput
     */
    protected $output = '';
    /**
     * @var string the template associated with this view
     */
    protected $template;
    /**
     * @var string The view this template is to be inserted after
     */
    protected $insertAfter;
    /**
     * @var string the view this template is to be inserted before
     */
    protected $insertBefore;
    /**
     * @var array the global data, handled by the parent request
     */
    protected $globalData;
    
    /**
     *
     * @var string name of the view
     */
    protected $viewName;
    
    /**
     * @var string name of parent view
     */
    protected $parentView;
    
   /**
    * Set the data associated with this view
    * @param mixed $data
    */
    public function setData($data){
        $this->data = $data;
    }
    
    /**
     * Provision the data.
     * This is a little messy at the moment but its so that legacy templates will still work
     * that access object data via $this
     */
    private function provisionData(){
        //loop through global data and make that accesible via $this
      if(is_array($this->globalData) || is_object($this->globalData)){
            foreach($this->globalData as $key => $item){
               $this->$key = $item;
           }
       }
       //loop through local data and make that accesible via $this - NOTE local data takes priority over global
       if(is_array($this->data) || is_object($this->data)){
           foreach($this->data as $key => $item){
               $this->$key = $item;
           }
       }
    }
    
    /**
     * Render this view. Sets $this->ouput and returns string.
     * @param The global data passed from the calling SiteResponce
     * @return string html
     */
    public function render($data){
        if(!$this->rendered){
            $this->globalData = $data;
            $this->provisionData();
            ob_start();
            $this; 			# allows this to be available within the template
            include($this->template);
            $contents = ob_get_contents();
            ob_end_clean();
            $this->output = $contents;
            
        }
        return $this->output;
    }
       
    /**
     * Setter for $this->rendered
     * @param boolean $rendered
     */
    public function setRendered($rendered){
        if(is_bool($rendered))
            $this->rendered = $rendered;
    }
    
    /**
     * Setter for $this->output
     * @param string $output
     */
    public function setOutput($output){
        if(is_string($output))
            $this->output = $output;
    }
    
    /**
     * Setter for $this->template
     * @param string $template
     */
    public function setTemplate($template){
        if(is_string($template))
            $this->template = $template;
    }
    
    /**
     * Setter for parameters for the view object
     * currently only concerned with insertAfter and insertBefore
     * @param array $params
     */
    public function setParams($params){
        if(isset($params['insertAfter']) && is_string($params['insertAfter'])) 
            $this->insertAfter = $params['insertAfter'];
        
        if(isset($params['insertBefore']) && is_string($params['insertBefore']))
            $this->insertBefore = $params['insertBefore'];
    }
    
    /**
     * getter for $this->insertBefore
     * @return string
     */
    public function getInsertBefore(){
        return $this->insertBefore;
    }
    
    /**
     * getter for $this->insertAfter
     * @return string
     */
    public function getInsertAfter(){
        return $this->insertAfter;
    }
    
    /**
     * getter for $this->viewName
     * @return string
     */
    public function getViewName(){
        return $this->viewName;
    }
    
    /**
     * Setter for $this->viewName
     * @param string $name
     */
    public function setViewName($name){
        if(is_string($name))
            $this->viewName = $name;
    }
    
    /**
     * Setter for $this->parentView
     * @param string $name
     */
    public function setParentView($name){
        if(is_string($name))
            $this->parentView = $name;
    }
    /**
     * Getter for $this->parentView
     * @return string
     */
    public function getParentView(){
        return $this->parentView;
    }
    
    public function removeParentView() {
        unset($this->parentView);
    }

    /**
     * Helper function to render children of parent views.
     */
    public function renderChildren(){
        return SiteResponse::getInstance()->renderChildViews($this->viewName);
    }
    
    /**
     * Helper function to render a single child of the parent view.
     * @param string $childViewName  View to render
     * @return string HTML
     */
    public function renderChild($childViewName){
        return SiteResponse::getInstance()->renderChildView(
            $this->viewName, $childViewName
        );
    }

    /**
     * function to add data to existing view data
     * @param mixed $newData
     * @return boolean success
     */
    public function addData($newData){
        if(!(is_array($newData) || is_object($newData)))
            return false;
        
        if(!isset($this->data)){
           $this->data = $newData;
        }elseif(is_array($this->data)){
            foreach($newData as $key => $item){
                $this->data[$key] = $item;
            }
        }elseif(is_object($this->data)){
            foreach($newData as $key => $item){
                $this->data->{$key} = $item;
            }
        }else{
            return false;
        }
        
        return true;
    }
}

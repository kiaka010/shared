<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class SharedExceptionHandler {

    protected $log;

    protected $env;

    private $errorString;

    protected static $response;

    public function __construct($application){
        $this->application = $application;
        self::$response = SharedResponse::getInstance();
    }

    public function handleException($exception) {

        $this->errorCode = $exception->getCode();
        http_response_code($this->errorCode);
        $this->errorMessage = $exception->getMessage();
        $this->traceString = $exception->getTraceAsString();
        $this->type = $this->getErrorType();
        $this->exception = $exception;

        // build error string
        $this->buildErrorString();

        //log error regardless

        // also log to apache error log
        error_log($this->errorString);

        switch($this->type){
            case 404:
                $this->handle404Error();
               break;
//            case 410:
//                $this->handle410Error();
//                break;
//            case 500:
//                $this->handle500Error();
//                break;
//            case 'Notice':
//                $this->showError();
//                return true;
//                break;
        }

    }

    private function buildErrorString(){
        $string = 'ERROR THROWN AND CAUGHT: '. $this->errorMessage . '
                    ' . get_class($this->exception)." thrown within " . $this->exception->getFile() . " on line ".$this->exception->getLine() . ".\n\n" .
            $this->exception->getTraceAsString();
        if(isset($_SERVER['REQUEST_URI'])) {
            $string .= "\n\nfrom : " . $_SERVER['REQUEST_URI'];
        }
        $this->errorString = $string;
    }

    private function handle404Error(){
        switch(ENV){
            case 'dev':
//            case 'stage':
            case 'uat':
                $this->showError();
            case 'live':
            default:
                $this->dispatchError('404');
        }
    }
//
//    private function handle410Error(){
//        switch($this->env){
//            case 'dev':
//            case 'stage':
//            case 'uat':
//                $this->showError();
//            case 'live':
//            default:
//                include DOC_ROOT .'/410.php';
//                $devLog =  htmlspecialchars(SkySportsLog::output(true));
//                include TEMPLATE_PATH . '/common/wrappers/dev.mvc.log.html.php';
//        }
//    }
//
//    private function handle500Error(){
////        switch($this->env){
////            case 'dev':
////            case 'stage':
////            case 'uat':
//                $this->showError();
////            case 'live':
////            default:
////                include DOC_ROOT .'/500.php';
////                $devLog =  htmlspecialchars(SkySportsLog::output(true));
////                include TEMPLATE_PATH . '/common/wrappers/dev.mvc.log.html.php';
////        }
//    }
//
    private function showError(){
        if($GLOBALS['isLive'] == 0)
            echo '<pre style="background:#fff;color:#000;padding:20px;">' . $this->errorString . '</pre>';
    }


    private function getErrorType() {
        switch ($this->errorCode) {
            case E_NOTICE:
            case E_USER_NOTICE:
                $type = 'Notice';
                break;
            case E_WARNING:
            case E_USER_WARNING:
                $type = 'Warning';
                break;
            case E_ERROR:
            case E_USER_ERROR:
                $type = 'Fatal Error';
                break;
            case 404:
                $type = 404;
                break;
            default:
                $type = $this->errorCode;
                break;
        }
        return $type;
    }

}
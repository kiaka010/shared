<?php

require_once SHARED_UTILS_PATH . '/CharUtils.php';
require_once SHARED_CLASS_PATH . '/core/mvc/SharedMain.php';

class SharedMain {

    public static $_router;
    public static $_response;
    public static $_request;
    public static $_config;
    protected static $config;
    public function load() {

        $targetController = $this->init();

        // throw error if controller is not an object (i.e. routing
        // didn't find a controller)
        if (!is_object($targetController)) {
            throw new Exception('Valid target controller not found', 404);
        }

        // inject our config array into controller
        unset(self::$config['routes']);
        $targetController->setConfig(self::$config);


        // dispatch the app
        $result = $this->dispatch($targetController);

        // render
        self::renderResponse($result);
    }

    public function init() {
        // define error handler
        $exceptionHandler = $this->getExceptionHandler();
        set_exception_handler(array($exceptionHandler, 'handleException'));

        self::$_router = $this->getRouter();
        self::$_request = $this->getRequestInstance();
        $defaultModule = self::$_request->getDefaultModule();

        self::$_config = $this->fetchConfig($defaultModule);
        // lets add the routes
        foreach (self::$_config['routes'] as $route) {
            self::$_router->addRoute($route);
        }

        $foundRoute = self::$_router->findRoute();

        return $foundRoute;
    }

    public function fetchConfig($module) {
        // the fallback config
        $config = require DOC_ROOT . '/site/common/config/module.config.php';
        if($module != 'common') {
            $moduleConfigFile = sprintf(
                DOC_ROOT . '/site/%s/config/module.config.php',
                CharUtils::folderize($module)
            );

            if (file_exists($moduleConfigFile)) {
                $moduleConfigFileData = include $moduleConfigFile;
                $config = array_merge_recursive($moduleConfigFileData, $config);
            }
        }

        return $config;
    }
        /**
     * Renders the response.
     * @param mixed $response
     */
    public static function renderResponse($response) {
        $cacheDuration = self::$_response->fetchCache();

        header("Cache-Control: max-age=$cacheDuration, s-maxage=$cacheDuration");
        header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$cacheDuration) . ' GMT');

        self::$_response->renderResponse();

    }

    protected function getResponceInstance() {
        return SharedResponse::getInstance();
    }

    public function dispatch($controller) {


        // get the singleton response
        self::$_response = $this->getResponceInstance();

        // inject request/response into controller
        $controller->setRequest(self::$_request);
        $controller->setResponse(self::$_response);

        // dispatch the controller action and return results
        return $controller->dispatch(
            self::$_request->getAction(),
            self::$_request->getParams()//,
            //self::$_request->getIsAjax()
        );

    }
    /**
     * Getter for config
     * @return array
     */
    public function getConfig() {
        return self::$config;
    }

    /**
     * Setter for config
     * @param array
     */
    public function setConfig($config) {
        self::$config = $config;
    }

    /** getters **/
    protected function getExceptionHandler() {
        return new SharedExceptionHandler($this);
    }

    protected function getRouter() {
        return new SharedRouter();
    }

    protected function getRequestInstance() {
        return SharedRequest::getInstance();
    }

}

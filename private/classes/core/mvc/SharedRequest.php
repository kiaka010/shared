<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SharedRequest {
    /**
     * @var object instance of $this - singleton 
     */
    public static $instance;
    protected $uriPath = null;
    protected $uriParts = null;
    
    protected $defaultModule = null;
    protected $defaultController = null;
    protected $defaultAction = null;
    protected $defaultParams = [];




    public static function getInstance(){
        if(!self::$instance instanceof SharedRequest) {
            //create an instance of this class
            self::$instance = new SharedRequest();
        }
       //return single instance
        return self::$instance;
    }
    
    public function __construct() {
        $this->buildRequest();
        $this->parseDefaults();
    }
    
    private function buildRequest() {
        $this->headers = $headers = array_change_key_case($_SERVER, CASE_LOWER);
        $uriPath = str_replace('?' . $this->headers['query_string'], '', $headers['request_uri']);
        if(substr($uriPath, -1) == '/') {
            $uriPath = rtrim($uriPath, '/');
        }
        
        $this->uriPath = $uriPath;
    }
    
    private function parseDefaults() {
        $urlParts = explode('/', $this->uriPath);
        //clear empty first array
        if(empty($urlParts[0])) {
            unset($urlParts[0]);
            $urlParts = array_values($urlParts);
        }
        //assume firt part is module
        if(!empty($urlParts[0])) $this->setDefaultModule($urlParts[0]);
        
        // assume second part is controller
        if(!empty($urlParts[1])) $this->setDefaultController($urlParts[1]);

        // assume third part is the action
        if(!empty($urlParts[2])) $this->setDefaultAction($urlParts[2]);
        
        $paramsFromUrl = $getParams = [];
        
        //assume any remaining aprts of the url are parameters
        for($i = 3; $i < count($urlParts) ; $i++){
           $paramsFromUrl[] = $urlParts[$i];
        }
        //check for any get params
        if(!empty($_GET)) {
            $getParams = $_GET;
        }
        unset($getParams['url']);
        $this->setDefaultParams(array_merge($paramsFromUrl, $getParams));
        
        $this->uriParts = $urlParts;
    }
             
    public function setModule($module){
        if(is_string($module))
            $this->module = $module;
    }
    /**
     * Setter for requested controller
     * @param string $controller
     */
    public function setController($controller){
        if(is_string($controller))
            $this->controller = $controller;
    }   
    /**
     * setter for requested action
     * @param type $action
     */
    public function setAction($action){
        if(is_string($action))
            $this->action = $action;
    }
 
    
    
    /**
     * Setter for request parameters
     *  @param array $params
     */
    public function setParams($params){
        if(is_array($params))
            $this->params = $params;
    }
    //setters
    public function setDefaultModule($module) {
        if(is_string($module)) {
            $this->defaultModule = $module;
        }
    }
    
    public function setDefaultController($controller) {
        if(is_string($controller)) {
            $this->defaultController = $controller;
        }
    }
    
    public function setDefaultAction($action) {
        if(is_string($action)) {
            $this->defaultAction = $action;
//                        $this->defaultAction = CharUtils::convertHyphenatedToCamelCase($action);

        }
    }
    
    public function setDefaultParams($params) {
        if(is_array($params)) {
            $this->defaultParams = $params;
        }
    }
    //getters
    
    public function getUriPath() {
        return $this->uriPath;
    }
    
    /**
     * Getter for default action
     * @return string
     */
    public function getDefaultAction(){
        return $this->defaultAction;
    }
    /**
     * Getter for default parameters
     * @return array
     */
    public function getDefaultParams(){
        return $this->defaultParams;
    }
    
    
    public function getMethod() {
//        return $this->fe
    }
    
    public function getAction($addPage = true) {
        if ($addPage) {
            return $this->action . 'Page';
        } else {
            return $this->action;
        }
    }
    
        /**
     *
     * checkForAppUri
     *
     * Check the requested URI for /app prefix.
     * If we have set isApp flag to true and strip the /app from URI
     * so we can continue processing URI as non-app version.
     */
    private function checkForAppUri(){
        $potentialAppPath =  substr($this->uriPath, 0, 4);
        if($potentialAppPath === '/app') {
            $this->isApp = true;
            $this->uriPath = substr($this->uriPath, 4);
        }
    }

    /**
     * Getter for the controller
     * @return string
     */
    public function getController(){
        return $this->controller;
    }
    /**
     * Getter for the requested module
     * @return string
     */
    public function getModule(){
        return $this->module;
    }

    /**
     * Getter for the requested subDir
     * @return string
     */
    public function getSubDir(){
        return $this->subDir;
    }
    /**
     * getter for the requested parameters
     * @return array
     */
    public function getParams(){
        return $this->params;
    }
    /**
     * getter for an individual parameter
     * @param string $key
     * @return mixed
     */
    public function getParam($key){
        return (isset($this->params[$key])) ? $this->params[$key] : null;
    }

    /**
     * setter for requested subDir
     * @param type $subDir
     */
    public function setSubDir($subDir){
        if(is_string($subDir))
            $this->subDir = $subDir;
    }

    
    /**
     * Set an individual parameter
     * @param string $key
     * @param string $value
     * @param boolean $forceOverwrite
     * @return mixed
     */
    public function setParam($key, $value, $forceOverwrite = false){
        if(is_string($key) && is_string($value)){
            if(isset($this->params[$key]) && !$forceOverwrite)
                return false;
            
            $this->params[$key] = $value;
        }
            
    }



    /**
     * Getter for default module
     * @return string
     */
    public function getDefaultModule(){
        return $this->defaultModule;
    }
    /**
     * Getter for default controller
     * @return string
     */
    public function getDefaultController(){
        return $this->defaultController;
    }
    
    /**
     * Getter for this request protocol
     * @return string
     */
    public function getProtocol(){
        return $this->protocol;
    }
    
    /**
     * Get requested url
     * 
     * will build the requested URL based on the current protocol, host and URI Path.
     * @return string
     */
    public function getRequestedUrl(){
        $url = $this->protocol . $this->host . $this->uriPath;
        # remove trailing slash if there is one
        if(substr($url, -1) == '/')
                $url = rtrim($url, '/');
        
        return $url;
    }
    
    /**
     * Setter to set the parameters for the request
     * Called by SkySportsRouter
     * @param array $params
     */
    public function setRequestParams($params){
        if(isset($params['module'])){
            $this->setModule($params['module']);
        }else{
            $this->setModule($this->getDefaultModule());
        }
        
        if(isset($params['controller'])){
            $this->setController($params['controller']);
        }else{
            $this->setController($this->getDefaultController());
        }
        
        if(isset($params['action'])){
            $this->setAction($params['action']);
        }else{
            $this->setAction($this->getDefaultAction());
        }
        
        if(isset($params['subDir'])){
            $this->setSubDir($params['subDir']);
        }else{
            $this->setSubDir('');
        }
        
        if(isset($params['params'])){
            $this->setParams($params['params']);
        }else{
            $this->setParams($this->getDefaultParams());
        }
        
        if($this->controller == 'ajax')
           $this->isAjax = true;
    }
    /**
     * Getter for $this->isAjax
     * @return boolean
     */
    public function getIsAjax(){
//        dump(debug_backtrace(null,2));
        return $this->isAjax;
    }
    /**
     * Getter for $this->uriParts
     * @return array
     */
    public function getUriParts(){
        return $this->uriParts;
    }
    
    /**
     * Get a specific URI part
     * @param integer $num
     * @return type
     */
    public function getUriPart($num){
        if(is_numeric($num) && isset($this->uriParts[$num]))
            return $this->uriParts[$num];
    }
    
    /**
     * Getter for $this->host
     * @return string
     */
    public function getHost(){
        return $this->host;
    }
    
    /**
     * Getter for the current request url
     * @return string
     */
    public function getRequestUrl(){
        return $this->host . $this->uriPath; 
    }
    
    /**
     * Setter for $this->uriPath, primarily for testing
     */
    function setUriPath($uriPath){
        $this->uriPath = $uriPath;
    }
    
    /**
     * Setter for $this->method, primarily for testing
     */
    function setMethod($method){
        $this->method = $method;
    }

    /**
     * Setter for $this->isApp, used to flag the URI as /app
     * We strip /app from URI in the constructor and access isApp()
     * is controllers to do app specific actions.
     */
    function isApp() {
        return $this->isApp;
    }
    
//    public function __construct() {
//        $this->processPath();
//    }
//
//    protected function processPath() {
//        $headers = $headers = array_change_key_case($_SERVER, CASE_LOWER);
//        $uriPath = str_replace('?' . $headers['query_string'], '', $headers['request_uri']);
//        if (substr($uriPath, -1) == '/') {
//            $uriPath = rtrim($uriPath, '/');
//        }
//        $urlParts = explode('/', $uriPath);
//        if ($urlParts[0] == '') {
//            unset($urlParts[0]);
//            $urlParts = array_values($urlParts);
//        }
//        $module = isset($urlParts[0]) ? $urlParts[0] : 'common';
//        $controller = isset($urlParts[1]) ? $urlParts[1] : 'index';
//        $action = isset($urlParts[2]) ? $urlParts[2] : 'index';
//                // assume any remaining parts of url are parameters
////        if(!empty($urlParts[3])) $paramsFromUrl = explode('/',$urlParts[3])
////        if(isset($urlParts[3])) {
////            $this->getParams
////        }
//}

}

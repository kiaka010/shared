<?php

class SharedRouter {

    protected $request;
    protected $routePaths;

    public function __construct() {
        $this->request = SharedRequest::getInstance();
    }

    public function addRoute($routeObj) {
        //check is an array and the first element is a string
        if (!is_array($routeObj) || !isset($routeObj[0]) || !is_string($routeObj[0])) {
            return false;
        }

        //break down the object to get the route path
        $route = $routeObj[0];
        //break down the object to get the route params
        $params = isset($routeObj[1]) ? $routeObj[1] : [];

        $this->routePaths[] = [$route, $params];
    }

    public function findRoute() {
        //get the uri path
        $uri = $this->request->getUriPath();
        foreach ($this->routePaths as $routeArr) {
            
            list($_route, $routeParams) = $routeArr;

            #is the route a negative one?
            if (isset($_route[0]) && $_route[0] === '!') {
                $negate = true;
                $i = 1;
            } else {
                $negate = false;
                $i = 0;
            }
            #@ is used to specify custom regex
            if (isset($_route[$i]) && $_route[$i] === '@') {
                #as its custom regex, just preg match it
                $match = preg_match('`' . substr($_route, $i + 1) . '`i', $uri, $params);

            } else {
                #Compiling and matching regular expressions is relatively
                #expensive, so try and match by a substring first
                $route = null;
                $regex = false;
                $j = 0;
                $n = isset($_route[$i]) ? $_route[$i] : null;

                #Find the longest non-regex substring and match it against the URI
                while (true) {
                    if (!isset($_route[$i])) {
                        break;
                    } elseif (false === $regex) {
                        $c = $n;
                        $regex = $c === '[' || $c === '(' || $c === '.';
                        if (false === $regex && false !== isset($_route[$i+1])) {
                            $n = $_route[$i + 1];
                            $regex = $n === '?' || $n === '+' || $n === '*' || $n === '{';
                        }
                        if (false === $regex && $c !== '/' && (!isset($uri[$j]) || $c !== $uri[$j])) {
                            continue 2;
                        }
                        $j++;
                    }
                    $route .= $_route[$i++];
                }

                #compile the regex
                $regex = $this->compileRoute($route);

                #preg match against the compiled regex
                $match = preg_match($regex, $uri, $params);
            }
        
            #Do we have a match?
            if(isset($match) && $match ^ $negate){

                #build route params / get defaults
                $routeParams = array_merge($routeParams, $params);
                $routeParams = $this->buildRouteParams($routeParams);
//                dump($routeParams);
                #does route exist?
                $check = $this->checkRouteParams($routeParams);
                #Ah-ha! We have something
                if ($check !== false) {
                    #set the request object to the correct parameters.
                    $this->request->setRequestParams($routeParams);
                    #return the target controller class
                    return $check;
                }
            }
            
        }
            return null;
    }
    /**
     * Checks that the route exists.
     * It expects page controllers to be in /<module>/controllers/<controller>Controller.php
     * and page actions to be <action>Page()
     * @param type $routeParams
     * @return object target controller class
     */
    public function checkRouteParams($routeParams){
        #initialise the response
        $response = false;
        #get our route params
        $_module = CharUtils::folderize($routeParams['module']);
        $_controller = CharUtils::camelize($routeParams['controller'],true);
        $_action = $routeParams['action'];
        $_subDir = $routeParams['subDir'];
        #build the expected module > controller filename
        if (!empty($_subDir)) {
            $fileName = $_module . DS . 'controllers' . DS . $_subDir . DS . ucfirst($_controller) . 'Controller.php';
        } else {
            $fileName = $_module . DS . 'controllers' . DS . ucfirst($_controller) . 'Controller.php';
        }
        $fileName = DOC_ROOT .'/site/' .  $fileName;
        #does our file exist?
        if(file_exists($fileName)){
            #require it!
            require_once $fileName;
            #build the class name
            $className = $_controller.'Controller';
            #does the class exist?
            if(class_exists($className)){
                #build the action method
                 $method = $_action.'Page';
                 #initiate the class
                 $controller = new $className;
                 #check the methid exists
                 if(method_exists($controller, $method)){
                     #bingo!! return the target controller
                     $response = $controller;
                 }
            }
        }

        if(!$response){
            # lets try and see if we have a common controller
            $fileName = 'common' . DS . 'controllers' . DS . 'Common' . ucfirst($_controller).'Controller.php';
            if(file_exists($fileName)){
                #require it!
                require_once $fileName;
                #build the class name
                $className = 'Common' . $_controller.'Controller';
                #does the class exist?
                if(class_exists($className)){
                    #build the action method
                     $method = $_action.'Page';
                     #initiate the class
                     $controller = new $className;
                     #check the methid exists
                     if(method_exists($controller, $method)){
                         #bingo!! return the target controller
                         $response = $controller;
                     }
                }
            }
        }
        return $response;
    }
    /**
     * Clean the routeParams array and get defaults for any that aren't defined within the config.
     * @param array $routeParams found route parameters
     * @return array formatted route parameters
     */
    public function buildRouteParams($routeParams) {
        #get module, controller, action and subDir actions
        $routeParams['module'] = isset($routeParams['module']) ? $routeParams['module'] : $this->request->getDefaultModule();
        $routeParams['controller'] = isset($routeParams['controller']) ? CharUtils::convertHyphenatedToCamelCase($routeParams['controller']) : $this->request->getDefaultController();
        $routeParams['action'] = isset($routeParams['action']) ? CharUtils::convertHyphenatedToCamelCase($routeParams['action']) : $this->request->getDefaultAction();
        $routeParams['subDir'] = isset($routeParams['subDir']) ? $routeParams['subDir'] : '';

        #anything else is passed as parameters to the function
        $parameters = array();
        foreach ($routeParams as $key => $var) {
            #if not a 'protected' param name then add it to the parameters of the action
            if (!in_array($key, array('module', 'controller', 'action', 'subDir')) || !$key) {

                if (preg_match('/[A-Za-z]/', $key)) {
                    if ($key == 'params') {
                        #need to explode by '/'
                        $exArr = explode('/', $var);
                        foreach ($exArr as $param) {
                            $parameters[] = $param;
                        }
                        unset($routeParams[$key]);
                    } else {
                        $parameters[$key] = $var;
                    }
                }
                unset($routeParams[$key]);
            }
        }
        $routeParams['params'] = $parameters;
        return $routeParams;
    }

    public function compileRoute($route) {
        if (preg_match_all('`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`', $route, $matches, PREG_SET_ORDER)) {
            $match_types = array(
                'i' => '[0-9]++',
                's' => '[a-zA-Z0-9_\-\.]+',
                'a' => '[0-9A-Za-z]++',
                'h' => '[a-f0-9]{32}', //used to check MD5 hashing
                'hs' => '[a-f0-9]{40}', //used to check SHA1 hashing
                'd' => '((([1-9]|[1-3][0-9])(th|rd|st|nd)|((0[1-9]|[12][0-9]|3[01])))[-][A-z]+[-](18|19|20)[0-9][0-9])', #date in the format of 24th-May-2013 or 02-Februaryy-2013
                'm' => '([a-zA-Z]+\-[0-9]{4})', #month, year e.g. september-2013
                '*' => '.+?',
                '**' => '.++',
                '' => '[^/]+?'
            );

            foreach ($matches as $match) {
                list($block, $pre, $type, $param, $optional) = $match;

                if (isset($match_types[$type])) {
                    $type = $match_types[$type];
                }
                if ($pre === '.') {
                    $pre = '\.';
                }
                #Older versions of PCRE require the 'P' in (?P<named>)
                $pattern = '(?:'
                    . ($pre !== '' ? $pre : null)
                    . '('
                    . ($param !== '' ? "?P<$param>" : null)
                    . $type
                    . '))'
                    . ($optional !== '' ? '?' : null);

                $route = str_replace($block, $pattern, $route);
            }
        }
        return "`^$route$`";
    }

}


<?php

require_once SHARED_CLASS_PATH . '/db/DBConfigInterface.php';

class DbConnect {

    protected $connection;
    protected $dbConfig;

    public function setConnectionDetails(DBConfigInterface $dbConfig) {
        $this->dbConfig = $dbConfig;
    }

    public function openConnection() {
        if (!$this->connection instanceof mysqli) {
            //create connection
            mysqli_report(MYSQLI_REPORT_STRICT);
            try {
                $this->connection = new mysqli(
                        $this->dbConfig->getHost(),
                        $this->dbConfig->getUserName(),
                        $this->dbConfig->getPassword(),
                        $this->dbConfig->getDatabase(),
                        $this->dbConfig->getPort()
                );
            } catch (mysqli_sql_exception $e) {
//                throw new Exception('Access Error', 404);
            }

            if ($this->connection->connect_error) {
                die("Connection failed: " . $this->connection->connect_error);
            }
            return $this->connection;
        }
        return $this->connection;
    }

    public function closeConnection() {
        if (isset($this->connection) && $this->connection instanceof mysqli) {
            $this->connection->close();
        }
    }

}

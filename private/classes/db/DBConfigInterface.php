<?php
abstract class DBConfigInterface{
    
    protected $host;
    protected $password;
    protected $database;
    protected $secure;
    protected $port;
    protected $userName;

    abstract function setHost();
    abstract function setPassword();
    abstract function setDatabase();
    abstract function setSecure();
    abstract function setPort();
    abstract function setUserName();
    abstract function buidConfig();


    public function getHost() {
        return $this->host;
    }
    
    public function getPassword() {
        return $this->password;
    }
    
    public function getDatabase() {
        return $this->database;
    }
    
    public function getSecure() {
        return $this->secure;
    }
    
    public function getPort() {
        return $this->port;
    }
    
    public function getUserName() {
        return $this->userName;
    }


    /*
     * define("HOST", "mysql.hostinger.co.uk");     // The host you want to connect to.
define("USER", "u102068191_admin");    // The database username. 
define("PASSWORD", "ziehaus");    // The database password. 
define("DATABASE", "u102068191_site");    // The database name.
define("SECURE", FALSE);    // FOR DEVELOPMENT ONLY!!!!
define("PORT", null);

     */
}


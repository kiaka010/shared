<?php

class DbConnectionConfig
{
    protected $host;
    protected $userName;
    protected $password;
    protected $database;
    protected $port;
    
    public function setHost($host)
    {
        $this->host = $host;
    }
    
    public function getHost()
    {
        return $this->host;
    }
           
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }
    
    public function getUserName()
    {
        return $this->userName;
    }
    
    public function setPassword($password, $hash = 'MD5')
    {
        if($hash == 'MD5') 
        {
            $this->password = md5($password);
        } else {
            throw New Exception('Invalid DB Hash', 500);
        }
        
    }
    
    public function getPassword($password, $decrypt = false)
    {
        if($decrypt)
        {
            return 'something here to decrypt';
        }
        
        if(!$decrypt)
        {
            return $password;
        }
    }
    
    public function setDatabase($database)
    {
        $this->database = $database;
    }
    
    public function getDatabase()
    {
        return $this->database;
    }
    
    public function setPort($port)
    {
        if(!is_int($port))
        {
            throw new Exception('Port Not Valid', 500);
        }
        
        $this->port = $port;
    }
        
    public function getPort()
    {
        return $this->port;
    }
}

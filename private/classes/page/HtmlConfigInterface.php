<?php

interface HtmlConfigInterface {
    
    public function getSiteTitle();
    public function getSiteDescription();
    public function getSiteH2();
    public function setSiteH2($h2);
}
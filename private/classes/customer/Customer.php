<?php

class Customer
{

    protected $forename;
    protected $surname;
    protected $email;
    protected $number;
    protected $information;
    protected $extra = [];

    public function setForename($forename)
    {
        $this->forename = $forename;
        return $this;
    }

    public function getForename()
    {
        return $this->forename;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     * @return Customer
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;

    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setNumber($number)
    {
        $this->number = $number;
        return $this;

    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setInformation($info)
    {
        $this->information = $info;
        return $this;

    }

    public function getInformation()
    {
        return $this->information;
    }

    public function setExtra($key, $value)
    {
        $this->extra[$key] = $value;
    }

    public function getExtraByKey($key)
    {
        return $this->extra[$key];
    }

    public function getExtras()
    {
        return $this->extra;
    }

    /**
     * safety net just in case
     *
     * @return string
     */
    public function getName()
    {
        return $this->getFullName();
    }

    public function getFullName()
    {
        $name = ucfirst($this->getForename());
        $surname = $this->getSurname();
        if (isset($surname)) {
            $name .= ' ' . ucfirst($surname);
        }
        return $name;
    }
}
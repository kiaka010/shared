<?php


class GlobalEmailClient {
    protected $reciever = [];
    protected $sender = [];
    protected $bcc = [];
    protected $cc = [];

    protected $replyTo = [];
    protected $subject;
    protected $message;
    protected $isHtmlMessage = false;
    public function __construct() {
    }

    public function setReciever($name, $email) {
        $this->reciever = [
            'name' => $name,
            'email' => $email
        ];
    }

    public function setReplyTo($name, $email) {
        $this->replyTo = [
            'name' => $name,
            'email' => $email
        ];
    }

    public function setSender($name, $email) {
        $this->sender = [
            'name' => $name,
            'email' => $email
        ];
    }

    public function setBcc($name, $email)
    {
        $this->bcc = [
            'name' => $name,
            'email' => $email
        ];
    }
    public function setCc($name, $email)
    {
        $this->cc = [
            'name' => $name,
            'email' => $email
        ];
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function isHTMLMessage($isHtml) {
        $this->isHTMLMessage = $isHtml;
    }

    protected function buildHeader() {
        $header = '';

        if($this->isHTMLMessage) {
            $header .= 'MIME-Version: 1.0' . "\r\n";
            $header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        }
        $header .= sprintf("From: %s <%s>\r\n", $this->sender['name'], $this->sender['email']);

        if(!empty($this->replyTo)) {
            $header .= sprintf("Reply-To: %s <%s>\r\n", $this->replyTo['name'], $this->replyTo['email']);

        }

        if(!empty($this->bcc)) {
            $header .= sprintf("Bcc: %s\r\n", $this->bcc['email']);

        }
        if(!empty($this->cc)) {
            $header .= sprintf("Cc: %s\r\n", $this->cc['email']);

        }
        return $header;

    }
    public function send() {

        $message = $this->message;
        $headers = $this->buildHeader();

        // send email
        $result = $this->sendMail(
            $this->reciever['email'],
            $this->subject,
            $message,
            $headers
        );

        return $result;

    }
    protected function sendMail($to, $subject, $message, $headers) {
        return mail($to, $subject, $message, $headers);
    }


}

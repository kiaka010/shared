<?php
require_once SHARED_CLASS_PATH . "/price/Price.php";
require_once SHARED_CLASS_PATH . "/price/PriceGroup.php";


class PriceFetcher
{
    protected $filePath;

    public function __construct($filePath)
    {
        if (!file_exists($filePath)) {
            throw new Exception('Price File Not Found', 404);
        }

        $this->filePath = $filePath;
    }

    public function fetch()
    {
        $priceListRaw = file_get_contents($this->filePath);
        $priceList = $this->decodePriceList($priceListRaw);
        return $this->processPriceList($priceList);

    }

    /**
     * ignores trademarks
     * @param $name
     * @return mixed|null
     */
    public function fetchByTreatmentName($name)
    {
        $priceList = $this->fetch();
        foreach ($priceList as $price) {
            if (strtolower($price->getName(false)) == str_replace('-', ' ', strtolower($name))) {
                return $price;
            }
        }
        return null;
    }

    private function decodePriceList($rawPriceList)
    {
        return json_decode($rawPriceList);
    }

    private function processPriceList($priceList)
    {
        $priceListObject = [];

        foreach ($priceList as $priceGroup) {
            $newPriceGroup = new PriceGroup();
            $newPriceGroup->setName($priceGroup->name)
                ->setDescription($priceGroup->description)
                ->setSummary($priceGroup->summary)
                ->setSumCharCount(isset($priceGroup->sumCharCount) ? $priceGroup->sumCharCount : 450)
                ->setImagePath($priceGroup->img)
                ->setSubImagePath($priceGroup->subImg);

            if (isset($priceGroup->isRegisteredTrademark)) {
                $newPriceGroup->setRegisteredTrademark($priceGroup->isRegisteredTrademark);
            }
            if(isset($priceGroup->sessions)) {
                foreach ($priceGroup->sessions as $prices) {

                    $price = new Price();
                    $price->setName($prices->name);
                    $price->setLength($prices->length);
                    $price->setPrice($prices->price);
                    $newPriceGroup->addPrice($price);

                }
            }
            $priceListObject[] = $newPriceGroup;
        }
        return $priceListObject;
    }


}
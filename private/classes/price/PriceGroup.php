<?php

require_once SHARED_CLASS_PATH . '/price/Price.php';

class PriceGroup
{
    protected $name;
    protected $imagePath;
    protected $subImagePath;
    protected $description;
    protected $summary;
    protected $sumCharCount;
    protected $prices = [];
    protected $isRegisteredTrademark = false;

    public function getName($checkTradeMark = true)
    {
        if($checkTradeMark && $this->isRegisteredTrademark()) {
            return $this->name . '&reg;';
        }
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getImagePath()
    {
        return $this->imagePath;
    }

    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
        return $this;
    }

    public function hasImage()
    {
        if(!$this->imagePath)
            return false;

        return isset($this->imagePath);
    }

    /**
     * @return mixed
     */
    public function getSubImagePath()
    {
        return $this->subImagePath;
    }

    /**
     * @param mixed $subImagePath
     * @return PriceGroup
     */
    public function setSubImagePath($subImagePath)
    {
        $this->subImagePath = $subImagePath;
        return $this;
    }

    public function hassubImage()
    {
        return isset($this->subImagePath);
    }

    public function getDescription($clean = false)
    {
        if ($clean) {
            return strip_tags($this->description);
        }
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getPrices()
    {
        return $this->prices;
    }

    public function addPrice(Price $price)
    {
        $this->prices[] = $price;
    }

    /**
     * @param mixed $summary
     * @return PriceGroup
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSumCharCount()
    {
        return $this->sumCharCount;
    }

    /**
     * @param mixed $sumCharCount
     * @return PriceGroup
     */
    public function setSumCharCount($sumCharCount)
    {
        $this->sumCharCount = $sumCharCount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @return boolean
     */
    public function isRegisteredTrademark()
    {
        return $this->isRegisteredTrademark;
    }

    /**
     * @param boolean $isRegisteredTrademark
     * @return PriceGroup
     */
    public function setRegisteredTrademark($isRegisteredTrademark)
    {
        $this->isRegisteredTrademark = $isRegisteredTrademark;
        return $this;
    }

    public function getMoreLink($buildHtml = false)
    {
        $link = sprintf('/treatments/%s', str_replace(' ', '-', strtolower($this->getName(false))));
        if ($buildHtml) {
            $link = sprintf('<a href="%s">... more info</a>', $link);
        }

        return $link;
    }
}
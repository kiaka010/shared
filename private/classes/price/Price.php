<?php

class Price {
    protected  $name;
    protected $price;
    protected $length;
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setPrice($price)
    {
        $this->price = $price;
    }
    
    public function getPrice()
    {
        return $this->price;
    }
    
    public function setLength($length)
    {
        $this->length = $length;
    }
    
    public function getLength()
    {
        return $this->length;
    }
    
}
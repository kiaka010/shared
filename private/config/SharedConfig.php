<?php
if (!defined('SHARED_CLASS_PATH')) {
    define('SHARED_CLASS_PATH', SHARED_DOC_ROOT . '/private/classes');
}

if (!defined('SHARED_TEMPLATE_PATH')) {
    define('SHARED_TEMPLATE_PATH', SHARED_DOC_ROOT . '/private/templates');
}

if (!defined('SHARED_CONFIG_PATH')) {
    define('SHARED_CONFIG_PATH', SHARED_DOC_ROOT . '/private/config');
}

if (!defined('SHARED_UTILS_PATH')) {
    define('SHARED_UTILS_PATH', SHARED_DOC_ROOT . '/private/utils');
}
define('DS', DIRECTORY_SEPARATOR);

/**
 * config for the shared area
 * 
 * only to ever contain config details pretaining to the shared area
 */
class SharedConfig {

    public function __construct() {
        
    }

}

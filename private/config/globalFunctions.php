<?php

if(!function_exists('dump')) {
    function dump($var, $die = false, $removePreviousOutput = false) {
        global $notLive;
        if ($notLive === false) {
            // i.e. we are live, excuse the double negative
            return;
        }
        if ($removePreviousOutput) {
            ob_end_clean();
            ob_start();
        }
        print(
            '<pre style="color: black;background-color:white;padding:12px;border:2px solid #000; '.
            'border-left: 0px; border-right:0px;">'
        );
        if (is_array($var) || is_object($var)) {
            print_r($var);
        } else {
            var_dump($var);
        }
        print('</pre>');
        if ($die) {
            die;
        }
    }
}